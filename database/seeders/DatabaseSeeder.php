<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder {
    
    public function run() {
        
        
        $users = array(
            array(
                'name' => 'Admin',
                'phone' => '987678',
                'password' => bcrypt('password'),
                'email' => 'admin@company.com',
                'is_active' => 1,
                'user_type' => 1,
            ),
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }
        
        dd('done seed');
    }
    
}
