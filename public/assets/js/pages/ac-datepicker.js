'use strict';
$(document).ready(function() {

    $(function() {
        $('input[name="visit"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            },
            minDate: moment(),
            singleDatePicker: true,
            showDropdowns: false,
        });
    });
    
    $(function() {
        $('input[name="birth"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            },
            maxDate: moment(),
            singleDatePicker: true,
            showDropdowns: false,
        });
    });
    
    $(function() {
        $('input[name="birthdayReg"]').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            },
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1910,
            maxYear: parseInt(moment().format('YYYY'),10),
            maxDate: moment(),
        });
    });
	
	
});