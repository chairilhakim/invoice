<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class User extends Authenticatable {
    
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function getInsertUser($data){
        try {
            DB::table('users')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateUsers($fieldName, $name, $data){
        try {
            DB::table('users')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllUser(){
        $sql = DB::table('users')
                    ->where('users.user_type', '=', 10)
                    ->where('users.is_active', '=', 1)
                    ->orderBy('users.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getEmailValidate($email){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('email', '=', $email)
                    ->first();
        return $sql;
    }
    
    public function getDetailUser($id){
        $sql = DB::table('users')
                    ->where('users.user_type', '=', 10)
                    ->where('users.is_active', '=', 1)
                    ->where('users.id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getEmailValidateEdit($username, $id){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('email', '=', $username)
                    ->where('id', '!=', $id)
                    ->first();
        return $sql;
    }
    
}
