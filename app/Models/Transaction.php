<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Transaction extends Model {
    
    public function getInsertTransaction($table, $data){
        try {
            $lastInsertedID = DB::table($table)->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateTransaction($table, $fieldName, $name, $data){
        try {
            DB::table($table)->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllTransaction(){
        $sql = DB::table('transaction')
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getDetailTransaction($id, $user_id){
        $sql = DB::table('transaction')
                    ->where('id', '=', $id)
                    ->where('user_id', '=', $user_id)
                    ->first();
        return $sql;
    }
    
    public function getAllItem($id){
        $sql = DB::table('item')
                    ->where('transaction_id', '=', $id)
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
}