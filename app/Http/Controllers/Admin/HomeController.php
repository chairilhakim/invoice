<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use File;
use Response;
use App\Models\User;
use App\Models\Company;

class HomeController extends Controller {
    
    public function __construct(){
        
    }
    
    public function getHomepage(){
        return abort(404);
    }
    
    public function getFront(){
        return redirect()->route('memberLogin');
    }
    
    public function getMemberLogin(){
        return view('admin.login');
    }
    
    public function postMemberLogin(Request $request){
        $email = $request->user_login;
        $password = $request->user_password;
        $userdata = array('email' => $email, 'password'  => $password, 'is_active' => 1);
        
        if (Auth::guard("web")->attempt($userdata, $request->remember)) {
                $request->session()->regenerate();
                return redirect()->route('admDashboard')
                        ->with('message', 'Welcome...')
                        ->with('messageclass', 'success');
        }
        return redirect()->route('memberLogin')
                ->with('message', 'Something Wrong with your Password or Username')
                ->with('messageclass', 'danger');
    }
    
    public function getDashboard(){
        $dataUser = Auth::user();
        if($dataUser->user_type == 10){
            return redirect()->route('userCompany');
        }
        return view('admin.dashboard')
                    ->with('dataUser', $dataUser);
    }
    
    public function getAdminLogout(Request $request) {
        $dataUser = Auth::user();
        Auth::logout();
        $request->session()->invalidate();
        return redirect()->route('memberLogin');
    }
    
    public function displayImage($filename){
        $path = storage_path('app/public/'.$filename);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
    
    public function getMemberRegister(){
        return view('admin.register');
    }
    
    public function postMemberRegister(Request $request){
        if($request->name == null || $request->email == null || $request->password == null || $request->phone == null){
            return redirect()->route('memberRegister')
                ->with('message', 'Data must not empty')
                ->with('messageclass', 'danger');
        }
        $modelUser = New User;
        $validateEmail = $modelUser->getEmailValidate($request->email);
        if($validateEmail != null){
           return redirect()->route('memberRegister')
               ->with('message', 'use another email')
               ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'is_active' => 1,
        );
        $modelUser->getInsertUser($dataInsert);
        return redirect()->route('memberLogin')
                ->with('message', 'Insert Successfully')
                ->with('messageclass', 'success');
    }
    
}