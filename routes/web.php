<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\Admin\HomeController@getFront')->name('frontLogin');

Route::get('/login_admin', 'App\Http\Controllers\Admin\HomeController@getMemberLogin')->name('memberLogin');
Route::post('/login_admin', 'App\Http\Controllers\Admin\HomeController@postMemberLogin');
Route::get('image/{filename}', 'App\Http\Controllers\Admin\HomeController@displayImage')->name('image.displayImage');
Route::get('/register', 'App\Http\Controllers\Admin\HomeController@getMemberRegister')->name('memberRegister');
Route::post('/register', 'App\Http\Controllers\Admin\HomeController@postMemberRegister');

Route::prefix('/')->group(function () {
    Route::get('/logout', 'App\Http\Controllers\Admin\HomeController@getAdminLogout')->middleware('auth');
    Route::get('/dashboard', 'App\Http\Controllers\Admin\HomeController@getDashboard')->name('admDashboard')->middleware('auth');
    
    Route::get('/transaction', 'App\Http\Controllers\Admin\MasteradminController@getAllTransaction')->name('listTransaction')->middleware('auth');
    Route::get('/add/transaction', 'App\Http\Controllers\Admin\MasteradminController@getAddTransaction')->name('addTransaction')->middleware('auth');
    Route::post('/add/transaction', 'App\Http\Controllers\Admin\MasteradminController@postAddTransaction')->middleware('auth');
    Route::get('/edit/transaction/{id}', 'App\Http\Controllers\Admin\MasteradminController@getEditTransaction')->name('EditCompany')->middleware('auth');
    Route::post('/edit/transaction', 'App\Http\Controllers\Admin\MasteradminController@postEditTransaction')->middleware('auth');
    Route::post('/rm/transaction', 'App\Http\Controllers\Admin\MasteradminController@postRemoveTransaction')->middleware('auth');
    Route::get('/invoice/transaction/{id}', 'App\Http\Controllers\Admin\MasteradminController@getInvoiceTransaction')->name('InvoiceCompany')->middleware('auth');
    
    //Ajax
    Route::get('/ajax/rm/transaction/{id}', 'App\Http\Controllers\Admin\AjaxController@getRemoveTransaction')->middleware('auth');
    Route::get('/ajax/add/item-list', 'App\Http\Controllers\Admin\AjaxController@getAddItem')->middleware('auth');
    
});
