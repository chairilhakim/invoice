@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">
                            Edit User
                            <a class="badge badge-primary" href="{{ URL::to('/') }}/users">back</a>
                        </h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                         <form class="login100-form validate-form" method="post" action="/edit/user">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" required="true" autocomplete="off" value="{{$getData->name}}">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" name="phone" autocomplete="off" value="{{$getData->phone}}">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="getid" value="{{$getData->id}}">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" required="true" autocomplete="off" value="{{$getData->email}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" autocomplete="off">
                                        <small id="passwordHelpBlock" class="form-text text-muted">empty, when not change</small>
                                    </div>
                                </div>
                                
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
