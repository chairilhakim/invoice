@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>
                            Transaksi
                            <a class="badge badge-primary" href="{{ URL::to('/') }}/add/transaction">tambah</a>
                        </h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered table-xs" id="simpletable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Invoice</th>
                                        <th>Deskripsi</th>
                                        <th>Tgl</th>
                                        <th>Total</th>
                                        <th>##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                        @foreach($getData as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$row->invoice}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->date}}</td>
                                                <td>{{$row->price}}</td>
                                                <td>
                                                    <a class="badge badge-info" href="/invoice/transaction/{{$row->id}}">invoice</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section("styles")
<link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css"/>
@endsection

@section("javascript")
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(document).ready(function() {
            $("#simpletable").DataTable({
                "paging": true,
                "lengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "all"]],
                "searching": true,
                "ordering": false,
                "info": false,
                "responsive": true,
            });
        });
    </script>
    <script type="text/javascript">
        $("#popUp").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
    </script>
@endsection
