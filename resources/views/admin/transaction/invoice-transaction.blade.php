@extends('layouts.admin.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header p-4">
                        <div class="float-left">
                            <h4 class="mb-0">Invoice #{{$getTrans->invoice}}</h4>
                            {{date('d M, Y', strtotime($getTrans->date))}}
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-sm-6">
                                <h6 class="mb-3">Deskripsi:</h6>
                                <h5 class="text-dark mb-1">{{$getTrans->name}}</h5>
                            </div>
                        </div>
                        <div class="table-responsive-sm">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @foreach($getItem as $row)
                                    <?php $no++; ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->qty}}</td>
                                        <td class="right">{{$row->price}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="align-middle" colspan="3">Total</td>
                                        <td>{{$getTrans->price}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop