@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">
                            Buat Transaksi
                            <a class="badge badge-primary" href="{{ URL::to('/') }}/transaction">kembali</a>
                        </h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                         <form class="login100-form validate-form" method="post" action="/add/transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Deskripsi</label>
                                        <input type="text" class="form-control" name="name_desc" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="transaction_date">Tanggal</label>
                                        <input type="text" class="form-control datepickerStart" id="date" name="date" placeholder="Tanggal" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label>Nama Item</label>
                                      <input type="text" class="form-control" name="name[]" required="true" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                      <label>Jumlah</label>
                                      <input type="text" class="form-control allownumericwithoutdecimal" name="qty[]" required="true" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                      <label>Harga</label>
                                      <input type="text" class="form-control allownumericwithoutdecimal" name="price[]" required="true" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            
                            <div id="newRow"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="button" class="btn btn-sm btn-block btn-info" id="addrow" value="Tambah" />
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section("styles")
    <link rel="stylesheet" href="/assets/css/bootstrap-datetimepicker.css"/>
@endsection

@section('javascript')
<script type="text/javascript">
    $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
</script>

<script type="text/javascript">

    $("#addrow").on("click", function () {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/ajax/add/item-list",
            success: function(url){
                    $("#newRow").append(url);
            }
        });
    });

    $(document).on('click', '#removeRow', function () {
        $(this).closest('#addPengadaan').remove();
    });
</script>

    <script src="/assets/js/moment.min.js"></script>
    <script src="/assets/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $('.datepickerStart').datepicker({
            format: 'yyyy-mm-dd',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            },
            endDate: '0',
        });
        
    </script>
@stop
