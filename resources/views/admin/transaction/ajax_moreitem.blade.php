<div class="row" id="addPengadaan">
    <div class="col-md-6">
        <div class="form-group">
          <label>Nama Item</label>
          <input type="text" class="form-control" name="name[]" required="true" autocomplete="off">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
          <label>Jumlah</label>
          <input type="text" class="form-control allownumericwithoutdecimal" name="qty[]" required="true" autocomplete="off">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
          <label>Harga</label>
          <input type="text" class="form-control allownumericwithoutdecimal" name="price[]" required="true" autocomplete="off">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>&nbsp;</label>
            <input type="button" class="form-control btn btn-sm btn-danger " value="hapus" id="removeRow">
        </div>
    </div>
</div>

